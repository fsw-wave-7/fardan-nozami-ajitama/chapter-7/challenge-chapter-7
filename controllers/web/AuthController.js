const { Admin } = require("../../models");
const passport = require("../../lib/passport");
class AuthController {
  login = (req, res) => {
    res.render("login", {
      judul: "login",
      content: "./content/login",
    });
  };

  doLogin = passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true,
  });

  whoami = (req, res) => {
    res.render("index", req.user.dataValues);
  };

  logout = (req, res) => {
    req.logout();
    res.redirect("/login");
  };

  register = (req, res) => {
    Admin.register(req.body).then(() => {
      res.redirect("/admins");
    });
  };
}

module.exports = AuthController;
