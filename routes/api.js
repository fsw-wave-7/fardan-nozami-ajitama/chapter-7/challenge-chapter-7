var express = require("express");
var api = express.Router();
const UserController = require("../controllers/api/UserController");
const AuthController = require("../controllers/api/AuthController");
const userController = new UserController();
const authController = new AuthController();
const restrictApi = require("../middlewares/restrict-api");

// Authentication
api.post("/login", authController.login);
api.post("/register", authController.register);

api.use(restrictApi);
// user
api.get("/user", userController.getUser);
api.get("/user/:id", userController.getDetailUser);
api.delete("/user/:id", userController.deleteUser);

// //biodata
api.post("/biodata", userController.insertBiodata);
api.put("/biodata/:id", userController.updateBiodata);

// history
api.post("/insert-history", userController.insertHistory);

module.exports = api;
