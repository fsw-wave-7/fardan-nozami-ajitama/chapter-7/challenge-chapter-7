const { User } = require("../../models");
const { successResponse } = require("../../helpers/response");

function format(user) {
  const { id, username } = user;
  return { id, username, accessToken: user.generateToken() };
}
class AuthController {
  register = (req, res) => {
    User.register(req.body)
      .then((data) => {
        res.json(successResponse(res, 201, data));
      })
      .catch((err) => res.send(err.message));
  };

  login = (req, res) => {
    User.authenticate(req.body)
      .then((user) => {
        res.json(format(user));
      })
      .catch((err) => res.send(err));
  };

  logout = (req, res) => {
    res.clearCookie("loginData");
    res.redirect("/");
  };
}

module.exports = AuthController;
