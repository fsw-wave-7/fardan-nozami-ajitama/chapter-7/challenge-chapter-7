"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Biodata",
      [
        {
          name: "Fardan",
          address: "jogja",
          user_id: "1",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Nozami",
          address: "Surabaya",
          user_id: "2",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Ajitama",
          address: "Bantul",
          user_id: "3",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Biodata", null, {});
  },
};
