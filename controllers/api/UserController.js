const { User, History, Biodata } = require("../../models");
const { successResponse } = require("../../helpers/response");

class UserController {
  getUser = (req, res) => {
    User.findAll().then((users) => {
      res.json(
        successResponse(res, 200, users, {
          total: users.length,
        })
      );
    });
  };

  getDetailUser = (req, res) => {
    User.findOne({
      where: { id: req.params.id },
      include: [
        {
          model: History,
          as: "history",
        },
        {
          model: Biodata,
          as: "biodata",
        },
      ],
    }).then((user) => {
      res.json(successResponse(res, 200, user));
    });
  };

  // biodata
  insertBiodata = (req, res) => {
    Biodata.create(req.body).then((user) => {
      res.json(successResponse(res, 201, user));
    });
  };

  updateBiodata = (req, res) => {
    Biodata.update(req.body, { where: { id: req.params.id } }).then((user) => {
      res.json(successResponse(res, 201, user));
      console.log(user);
    });
  };

  // history
  insertHistory = (req, res) => {
    History.create(req.body).then((score) => {
      res.json(successResponse(res, 201, score));
    });
  };

  deleteUser = (req, res) => {
    User.destroy({
      where: {
        id: req.params.id,
      },
    });
    History.destroy({
      where: {
        user_id: req.params.id,
      },
    });
    Biodata.destroy({
      where: {
        user_id: req.params.id,
      },
    }).then(() => {
      res.json(successResponse(res, 200, null));
    });
  };
}

module.exports = UserController;
