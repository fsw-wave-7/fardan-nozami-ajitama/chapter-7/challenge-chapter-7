"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Histories",
      [
        {
          score: "10",
          roomId: "room001",
          user_id: "1",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: "10",
          roomId: "room001",
          user_id: "2",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: "20",
          roomId: "room002",
          user_id: "2",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: "0",
          roomId: "room002",
          user_id: "3",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: "0",
          roomId: "room002",
          user_id: "1",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: "20",
          roomId: "room002",
          user_id: "3",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Histories", null, {});
  },
};
