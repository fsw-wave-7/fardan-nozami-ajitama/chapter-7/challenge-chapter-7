"use strict";
const bcrypt = require("bcrypt");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const encrypt = (password) => bcrypt.hashSync(password, 10);
    await queryInterface.bulkInsert(
      "Users",
      [
        {
          username: "user",
          password: encrypt("user"),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "user2",
          password: encrypt("user2"),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "user3",
          password: encrypt("user3"),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Users", null, {});
  },
};
